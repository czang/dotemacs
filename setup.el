(message "Tangling init.el...")
(require 'org)
(org-babel-tangle-file "~/.emacs.d/init.org")
(message "Done.\n")

(message "installing packages from the Emacs package archives...")
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

(package-initialize)
(package-refresh-contents)

(setq my-packages
      '(smex
        auto-complete
        autopair
        ido-ubiquitous
        zenburn-theme
        magit
        auctex
        markdown-mode
        smart-mode-line
        anzu
        matlab-mode
        rainbow-delimiters
        smooth-scrolling
        ace-jump-mode
        expand-region
        js2-mode
        deft
        nodejs-repl
        dired-details
        ))

(mapc 'package-install my-packages)
